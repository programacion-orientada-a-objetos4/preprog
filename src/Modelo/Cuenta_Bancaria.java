/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Hp
 */
public class Cuenta_Bancaria {

    private Cliente cli;
    private int numCuenta = 0;
    private String fechaAper = "", nomBanco = "";
    private float porcRend = 0, saldo = 0;

    public Cliente getCli() {
        return cli;
    }

    public void setCli(Cliente cli) {
        this.cli = cli;
    }

    public int getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(int numCuenta) {
        this.numCuenta = numCuenta;
    }

    public String getFechaAper() {
        return fechaAper;
    }

    public void setFechaAper(String fechaAper) {
        this.fechaAper = fechaAper;
    }

    public String getNomBanco() {
        return nomBanco;
    }

    public void setNomBanco(String nomBanco) {
        this.nomBanco = nomBanco;
    }

    public float getPorcRend() {
        return porcRend;
    }

    public void setPorcRend(float porcRend) {
        this.porcRend = porcRend;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    public void depositar(float cant) {
        setSaldo(cant);
    }

    public boolean retirar(float resta) {
        if (resta <= saldo) {
            return true;
        } else {
            return false;
        }
    }

    public float calcRedi() {
        return (getPorcRend() * getSaldo()) / 365f;
    }
}
